# cairo-dock-weather-plug-in

Weather plug-in for Cairo-dock https://tracker.debian.org/pkg/cairo-dock-plug-ins

* [*CairoDock*
  ](https://help.ubuntu.com/community/CairoDock)
* cairo-dock looks unmaintened... but has a Debian maintener.

# Bug
## Cannot find meteo data
Apparently the URL for meteo data encoded in C source has changed.
* Version: 3.4.1
* [*Cairo-Dock weather applet not working*
  ](https://askubuntu.com/questions/774438/cairo-dock-weather-applet-not-working)
* [*Weather App - No Data/Retry error after 3.4 update*
  ](http://www.glx-dock.org/bg_topic.php?t=8945)
* [*Since yesterday there is no weather data available*
  ](https://bugs.launchpad.net/cairo-dock-plug-ins/+bug/1581725)
* `xfce4-weather-plugin` can be an alternative.